#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_SDW.mk

COMMON_LUNCH_CHOICES := \
    lineage_SDW-user \
    lineage_SDW-userdebug \
    lineage_SDW-eng
